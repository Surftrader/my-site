FROM openjdk:8
RUN mkdir -p /usr/app/
COPY ./target/app-docker.jar /usr/app/
COPY ./src/main/resources/static/download/app-nutrition-balance.apk /usr/app/
COPY ./src/main/resources/static/download/app_currency.apk /usr/app/
WORKDIR /usr/app/
EXPOSE 8086
ENTRYPOINT ["java", "-jar", "app-docker.jar"]
